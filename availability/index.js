const request = require('request');

const interval = setInterval(check, 100);

function check() {
    const options = { url: 'http://server:3000/', time: true };
    request.get(options, (err, res, body) => {
        if (err) {
            console.log(err);
        }
        console.log(res.elapsedTime);
    });
}