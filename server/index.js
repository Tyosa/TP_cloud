const express = require('express');

// initialize express
const app = express();
app.use(express.json());

class Player {
    id;
    history = []; // { player: 0, server :0 }
    balance = 300;
    debitor = false;
    won = false;

    constructor(id) {
        this.id = id;
    }
}

let players = new Map();

// players will post their chosen value to the server, which will generate its own and answer to the player if he won or not 
app.post("/", (req, res) => {

    // Retrieve the player
    // If the player does not exist yet, create it
    if (players.get(req.body.playerId) === undefined) {
        players.set(req.body.playerId, new Player(req.body.playerId));
    }
    let player = players.get(req.body.playerId);
    player.balance -= 10;

    let serverValue = generateValue();

    // check if the player won
    let won = false;
    if (pgcd(req.body.value, serverValue) == 1) { // si le pgcd de v1 et v2 est 1, les nombres sont premiers entre eux
        won = true;
        player.balance += 100;
    }
    if (player.balance >= 1000) {
        player.won = true;
    }
    if (player.balance <= 0) {
        player.debitor = true;
    }

    // generate server value and update player history
    player.history.push({ player: req.body.value, server: serverValue, won: won, balance: player.balance });

    res.send(`The match was ${req.body.value} vs ${serverValue} and you ${won ? 'won' : 'lost'}. 
                Your balance is ${player.balance}.
                ${player.won ? `You won the game! Here is your history : \n${historyToString(player.history)}` : ''}
                ${player.debitor ? 'You are a debitor!' : ''}`);
});

// generate a value between 2 and 200
function generateValue() {
    return Math.floor(Math.random() * 200) + 2;
}

// compute pgcd
function pgcd(a, b) {
    if (b > a) {
        var tmp = a;
        a = b;
        b = tmp;
    }
    while (true) {
        if (b == 0) return a;
        a %= b;
        if (a == 0) return b;
        b %= a;
    }
}

// convert history to string
function historyToString(history) {
    let str = '';
    for (let i = 0; i < history.length; i++) {
        str += `${history[i].player} vs ${history[i].server} and you ${history[i].won ? 'won' : 'lost'}. Your balance was ${history[i].balance}. \n`;
    }
    str += `You took ${history.length} turns to win.`;
    return str;
}


// run express
app.listen(3000, () => {
    console.log('Server running on port 3000');
});