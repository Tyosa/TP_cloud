# Oneshot Cloud

## Conception micro-services

Schéma simple (sans prendre en compte la scalabilité) : 

![Schema simple](./schema_simple.png)


Schéma avec prise en compte de la scalabilité (exemple, dépend de la charge): 

![Schema complexe](./schema_complexe.png)

Prix d'un seul microservice (21h/jour, 6j/semaine, 52 semaines dans l'année)= 0.00065\*21\*6\*52 = 4.2588$ / an

Premier cas -> 2 microservices back -> prix = 2*4.2588 = 8.5176$ / an.

Deuxième cas -> dépend de l'échelle qu'on prend, mais au minimum 3 microservices (load balancer + serveur + bdd) -> prix = 3*4.2588 = 12.6352$ / an.

Dans le premier cas le composant qui peut représenter un goulot d'étranglement est le serveur, car il doit prendre compte toutes les requêtes, mais aussi la base de données car elle prend aussi en compte ces requêtes.

Dans le deuxième cas, le load balancer peut représenter un goulot d'étranglement, et il faut bien penser à prendre en compte la réplication des données si on veut utiliser plusieurs services de base de données.

## Déploiement avec des containers

On déploie dans un premier temps avec des conteneurs, en enlevant la gestion de la base de données par manque de temps. La BDD est donc représentée par une variable `players` dans le serrveur.

Lancer le serveur (depuis le dossier serveur): `docker build -t tp-server .` puis `docker run -d -p 3000:3000 --network="test" --name server tp-server` 

Lancer un client (depuis le dossier client) : `docker build -t tp-client .` puis `docker run -d --network="test" tp-client`

### Test de l'implémentation avec 2 joueurs

Lancement du serveur et de X joueurs : `docker-compose up -d --scale client=X` 

Au cours de 10 tests sur 2 joueurs, les joueurs ont gagné en moyenne en 15.1 tours pour le premier et 16.2 tours pour le deuxième. Dans mes essais ils n'ont jamais été débiteurs.

## Passage à l'échelle

On met en place un composant permettant de vérifier le temps de réponse du serveur. Ce composant (placé dans le dossier availability) va juste faire une requête GET et logger dans la console le temps de réponse en millisecondes.

On observe les valeurs suivantes de latences :

- 10 clients : autour de 10ms
- 20 clients : autour de 10ms, quelques pics à 20-30ms de temps en temps
- 40 clients : autour 50ms
- 60 clients : entre 50 et 100 ms, parfois plus
- 120 clients : pas réussi à atteindre ce nombre de clients, mon client docker a crash en les lançant. J'ai quand même pu voir un pic à 500ms à un moment.

On observe un goulot d'étranglement important sur le serveur. Malheureusement avec le choix de ne pas utiliser de BDD (mais juste un tableau JS) je ne peux pas rendre le serveur scalable. L'idée serait de :

- créer deux (ou plus) serveurs (on peut le faire dans le docker-compose pour leur donner des noms différents)
- mettre en place un load balancer nginx comme on a vu en cours, qui accepte les requêtes sur / et renvoie vers un des deux serveurs (vers http://serverX:3000, en supposant que les serveurs écoutent sur le serveur 3000)

Théoriquement, cette solution permet de réduire grandement la charge sur les serveurs, car même en attribuant simplement le même poids à chaque serveur lors de la sélection ils auraient une charge divisée par deux.

Pour faire cela, il faudrait extraire la base de donnée pour en faire un microservice à part que les serveurs pourraient appeler.