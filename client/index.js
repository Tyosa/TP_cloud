const request = require('request');

const playerId = generateId();
console.log(`Joining the game as player ${playerId}`);

const interval = setInterval(sendMessage, 1000);

function sendMessage() {
    const options = {
        url: 'http://server:3000/',
        json: true,
        body: {
            playerId,
            value: generateValue()
        }
    }
    request.post(options, (err, res, body) => {
        if (err) {
            console.log(err);
        }
        console.log(body);
        if (body.includes("You won the game!")) {
            clearInterval(interval);
        }
    });
}


// generate a value between 2 and 200
function generateValue() {
    return Math.floor(Math.random() * 200) + 2;
}

// generate a value between 1 and 1000000
function generateId() {
    return Math.floor(Math.random() * 1000000) + 1;
}